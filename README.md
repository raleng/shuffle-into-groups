This script separates items from three different lists into groups.

Each groups should contain at least one element of the `base` list. The number of the items in the `base` and the `main` list are used to derive the number of groups needed according to the `minimalGroupSize`.
The rest of the `base` list and the `main` list is then randomly added to the groups.
The items of the `optional` list are added when all other items are distributed into the groups. The `minimalGroupSize` should not depend on whether an `optional` item is in a group or not. We accept rather bigger groups than a group that is smaller than the `minimalGroupSize` when an optional item is missing.
