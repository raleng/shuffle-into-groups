import           Control.Monad
import           Data.Array.IO
import qualified Data.Text     as Text
import qualified Data.Text.IO  as Text
import           System.Random

minimalGroupSize :: Int
minimalGroupSize = 3

baseFile :: String
baseFile = "base.txt"

mainFile :: String
mainFile = "main.txt"

optionalFile :: String
optionalFile = "optional.txt"

readLinesFromFile :: String -> IO [Text.Text]
readLinesFromFile file = fmap Text.lines (Text.readFile file)

shuffle :: [a] -> IO [a]
shuffle xs = do
    ar <- newArray n xs
    forM [1..n] $ \i -> do
        j <- randomRIO (i,n)
        vi <- readArray ar i
        vj <- readArray ar j
        writeArray ar j vi
        return vj
    where
        n = length xs
        newArray :: Int -> [a] -> IO (IOArray Int a)
        newArray n xs = newListArray (1,n) xs

joinByPlus :: [Text.Text] -> Text.Text
joinByPlus = Text.intercalate (Text.pack " + ")

formatWithGroup :: Int -> [Text.Text] -> [Text.Text]
formatWithGroup n (x:[]) = formatGroupText n x
formatWithGroup n (x:xs) = formatGroupText n x ++ formatWithGroup (n+1) xs

formatGroupText :: Int -> Text.Text -> [Text.Text]
formatGroupText n x = [(Text.pack "Group "), (Text.pack $ show n), (Text.pack ": "), x, (Text.pack "\n")]

addToNestedLists :: [[a]] -> [a] -> [[a]]
addToNestedLists (x:xs) (y:ys) = [x ++ [y]] ++ addToNestedLists xs ys
addToNestedLists xs []         = xs

addRestToGroups :: [[a]] -> [a] -> [[a]]
addRestToGroups groups [] = groups
addRestToGroups groups rest = fullGroups where
    numberOfGroups = length groups
    toAdd = take numberOfGroups rest
    newGroups = addToNestedLists groups toAdd
    fullGroups = addRestToGroups newGroups (drop numberOfGroups rest)

beginsWithHash :: Text.Text -> Bool
beginsWithHash x = (Text.head x) == '#'

main = do
    base <- readLinesFromFile baseFile
    let baseFiltered = filter (not . beginsWithHash) base

    main <- readLinesFromFile mainFile
    let mainFiltered = filter (not . beginsWithHash) main

    let numberOfGroups = div (length $ baseFiltered ++ mainFiltered) minimalGroupSize

    shuffledBase <- shuffle baseFiltered
    let (groupHeads, restBase) = splitAt numberOfGroups shuffledBase
    let baseGroups = map (\x -> [x]) groupHeads

    shuffledMain <- shuffle mainFiltered
    shuffledOptional <- readLinesFromFile optionalFile >>= shuffle
    let shuffledOptionalFiltered = filter (not . beginsWithHash) shuffledOptional

    -- Fill the base groups with the rest from the base and the main
    let groups = addRestToGroups baseGroups (restBase ++ shuffledMain)
    -- We shuffle the groups before adding the optionals to not have an optional as the first element
    shuffledGroups <- mapM shuffle groups
    -- The first groups might have one element more than the last ones, adding optionals from the top results in
    -- two element differences. Reversing the groups fixes that, but might leave groups in the middle with one
    -- less than the other groups
    let reversedShuffledGroups = reverse shuffledGroups
    -- Add optionals
    let shuffledGroupsWithOptional = addRestToGroups reversedShuffledGroups shuffledOptionalFiltered

    Text.putStr $ Text.concat $ formatWithGroup 1 $ map joinByPlus shuffledGroupsWithOptional
